import styles from './Header.module.css';

// TODO: fix labels and stuff for score, name, level and lives. 

const Header = () => {
    return (
        <header className={styles.main_header}>
            <div className={styles.header_content}>
                <div className={styles.score}>
                    SCORE
                </div>
                <div className={styles.title_and_level}>
                    <div>
                        <h1>ReactAnoid</h1>
                    </div>
                    <div>
                        Level
                    </div>
                </div>
                <div className={styles.lives}>
                    Lives
                </div>
            </div>
        </header>
    );
}

export default Header;