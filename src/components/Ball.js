import classes from './Ball.module.css'

const Ball = ({radius, position: { x, y }, speed}) => {
    const style = {
        position: 'absolute',
        width: `${radius*2}px`,
        height: `${radius*2}px`,
        left: `${x-radius}px`,
        top: `${y-radius}px`
    };
    return (
        <div className={classes.ball} style={style}></div>
    );
}

export default Ball;