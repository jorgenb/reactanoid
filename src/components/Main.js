import Ball from "./Ball";
import Level from "./Level";

import classes from './Main.module.css';
import Paddle from "./Paddle";

const Main = ({world, level, ball, paddle}) => {

    const styles = {
        width: `${world.width}px`,
        height: `${world.height}px`
    }

    return (
        <div className={classes.main_content}>
            <main>
                <div className={classes.playfield} style={styles}>
                    <Level level={level} />
                    <Paddle {...paddle} />
                    <Ball {...ball} />
                </div>
            </main>
        </div>
    );
}

export default Main;