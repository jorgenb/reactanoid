import { useState } from "react";
import { useAnimationFrame } from "../hooks/useAnimationFrame";
import { useBall } from "../hooks/useBall";
import { useLevel } from "../hooks/useLevel";
import { usePaddle } from "../hooks/usePaddle";
import { useSimulator } from "../hooks/useSimulator";
import { WORLD_HEIGHT, WORLD_WIDTH } from "../worldCfg";
import Header from "./Header";
import Main from "./Main";

import stars from '../images/stars.jpg'; //https://unsplash.com/photos/0o_GEzyargo

import classes from "./ReactAnoid.module.css";



const ReactAnoid = () => {

    const [keyPressed, setKeyPressed] = useState(0);
    const [paddle, setPaddle, movePaddle] = usePaddle();
    const [ball, setBall, resetBall] = useBall(paddle);
    const [level, setLevel] = useLevel();
    const [step] = useSimulator(ball, setBall, resetBall, paddle, movePaddle, keyPressed, level, setLevel);
    

    //const keyRef= useRef();
    //keyRef.current = keyPressed;

    useAnimationFrame(deltatime => {
        step(deltatime);
    });

    const onKeyDownHandler = ({ keyCode }) => {

        if (keyCode === 37) {
            setKeyPressed(37);
        }
        if (keyCode === 39) {
            setKeyPressed(39);
        }

    }

    const onKeyUpHandler = ({keyCode}) => {
        if(keyCode === 37 || keyCode === 39) {
            setPaddle(prev => {
                return {
                    ...prev,
                    speed: 0
                }
            });
            setKeyPressed(0);
        } 
    }

    return (
        <div className={classes.reactanoid} style={{backgroundImage: `url(${stars})`, backgroundSize: 'cover'}} onKeyDown={onKeyDownHandler} onKeyUp={onKeyUpHandler} role="button" tabIndex="0">
            <Header />
            <Main world={({ height: WORLD_HEIGHT, width: WORLD_WIDTH })} level={level} ball={ball} paddle={paddle} />
        </div>
    );
}

export default ReactAnoid;