import Block from "./Block";
import styles from './Level.module.css';

const Level = ({level}) => {

    const blocks = level.map((row, rowNumber) => row.map((column, columnNum) => {
        return <Block key={`${rowNumber}.${columnNum}`} value={column}/>
    }))

    return (
        <div className={styles.level}>
            { blocks }
        </div>
    );
}

export default Level;