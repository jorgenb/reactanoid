const Paddle = ({size: {width, height}, position: {x, y}}) => {
    const styles = {
        position: 'absolute',
        backgroundColor: '#00F',
        top: `${y - (height/2)}px`,
        left: `${x - (width/2)}px`,
        width: `${width}px`,
        height: `${height}px`,
        borderRadius: '5px'
    }
    return (
        <div style={styles}></div>
    );
}

export default Paddle;