import { getColor } from "../helpers/colorizer";

const Block = (props) => {
    const styles = {
        backgroundColor: getColor(props.value),
        borderTop: props.value === 0 ? 'none':'1px solid rgba(0, 0, 0, 0.2)',
        borderLeft: props.value === 0 ? 'none':'1px solid rgba(0, 0, 0, 0.2)',
        borderBottom: props.value === 0 ? 'none':'1px solid rgba(0, 0, 0, 0.6)',
        borderRight: props.value === 0 ? 'none':'1px solid rgba(0, 0, 0, 0.6)'
    }
    return (
        <div style={styles}></div>
    );
}

export default Block;