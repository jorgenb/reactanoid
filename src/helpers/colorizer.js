export const getColor = (blockvalue) => {
    if(blockvalue === 0) return 'transparent';
    if(blockvalue === 1) return '#fcfcfc';
    if(blockvalue === 2) return '#fc7460';
    if(blockvalue === 3) return '#3cbcfc';
    if(blockvalue === 4) return '#80d010';
    if(blockvalue === 5) return '#d82800';
    if(blockvalue === 6) return '#0070ec';
    if(blockvalue === 7) return '#fc74b4';
    if(blockvalue === 8) return '#fc9838';
    if(blockvalue === 9) return '#bcbcbc';
    return 'f0bc3c';
}
