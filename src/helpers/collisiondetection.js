import { doIntersect } from "./lineintersection";

export const blockBottomWallIntersect = (p1, q1, pRect, circle) => {
    if (doIntersect(p1, q1, {
        x: pRect.left - circle.radius,
        y: pRect.bottom + circle.radius
    }, {
        x: pRect.right + circle.radius,
        y: pRect.bottom + circle.radius
    })) {
        return true;
    }
    return false;
}

export const blockTopWallIntersect = (p1, q1, pRect, circle) => {
    if (doIntersect(p1, q1, {
        x: pRect.left - circle.radius,
        y: pRect.top - circle.radius
    }, {
        x: pRect.right + circle.radius,
        y: pRect.top - circle.radius
    })) {
        return true;
    }
    return false;
}

export const blockLeftWallIntersect = (p1, q1, pRect, circle) => {
    if (doIntersect(p1, q1, {
        x: pRect.left - circle.radius,
        y: pRect.top - circle.radius
    }, {
        x: pRect.left - circle.radius,
        y: pRect.bottom + circle.radius
    })) {
        return true;
    }
    return false;
}

export const blockRightWallIntersect = (p1, q1, pRect, circle) => {
    if (doIntersect(p1, q1, {
        x: pRect.right + circle.radius,
        y: pRect.top - circle.radius
    }, {
        x: pRect.right + circle.radius,
        y: pRect.bottom + circle.radius
    })) {
        return true;
    }
    return false;
}