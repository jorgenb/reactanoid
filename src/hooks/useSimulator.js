import { useRef } from "react";
import { blockBottomWallIntersect, blockLeftWallIntersect, blockRightWallIntersect, blockTopWallIntersect } from "../helpers/collisiondetection";
import { WORLD_HEIGHT, WORLD_WIDTH } from "../worldCfg";

export const useSimulator = (_ball, setBall, resetBall, _paddle, movePaddle, _keyPressed, _level, setLevel) => {
    const paddleRef = useRef();
    paddleRef.current = _paddle;
    const levelRef = useRef();
    levelRef.current = _level;
    const ballRef = useRef();
    ballRef.current = _ball;
    const keyRef = useRef();
    keyRef.current = _keyPressed;

    const SIXTY_DEGREES_RADIAN = 1.0471975511965976; //Math.PI / 3

    const step = (deltatime) => {
        const paddle = paddleRef.current;
        const level = levelRef.current;
        const paddlePos = paddle.position;
        const ball = ballRef.current;
        const keyPressed = keyRef.current;


        if(keyPressed === 37) { movePaddle(-1) }
        if(keyPressed === 39) { movePaddle(1) }
       // if(paddle.speed !== 0) console.log(paddle.speed);

        let newDX = ball.speed.dx;
        let newDY = ball.speed.dy;
        let newX = ball.position.x;
        let newY = ball.position.y;
        let collisions = [];
        let lostLife = false;

        ({ newDX, newDY, lostLife } = handleWallCollison(ball, newDX, newDY));
        ({ newDX, newDY, newX } = handleBallPaddleCollision(ball, paddle, newDX, newDY));
        ({ newDX, newDY, collisions } = handleBlockCollision(ball, newDX, newDY, level));

        if(lostLife) resetBall(paddlePos);
        else
        setBall(prevBall => {
            return {
                ...prevBall,
                position: {
                    ...prevBall.position,
                    x: prevBall.position.x + newDX,
                    y: prevBall.position.y + newDY
                },
                speed: {
                    ...prevBall.speed,
                    dx: newDX,
                    dy: newDY
                }
            };
        });

        /*if(paddle.speed !== 0) {
            setPaddle(prevPaddle => {
                return {
                    ...prevPaddle,
                    position: {
                        ...prevPaddle.position,
                        x: paddleNewX(prevPaddle),
                    },
                }
            })
        }*/

        if(collisions.length > 0) {
            setLevel(prev => {
                var newLevel = JSON.parse(JSON.stringify(prev));
                collisions.forEach(element => {
                    newLevel[element.rowIndex][element.columnIndex] = 0;
                })
                return newLevel;
            });
        }
    }

    //HELPERS
    const handleWallCollison = (prevBall, newDX, newDY) => {
        let lostLife = false;
        if (hitSideWalls(prevBall, newDX))
            newDX = -newDX;
        if (hitTopWall(prevBall, newDY)) 
            newDY = -newDY;
        if (hitBottomWall(prevBall, newDY)) {
            lostLife = true;
        }
        return { newDX, newDY, lostLife };
    }

    const hitBottomWall = (prevBall, newDY) => {
        return prevBall.position.y + prevBall.radius + newDY > WORLD_HEIGHT;
    }

    const hitTopWall = (prevBall, newDY) => {
        return prevBall.position.y - prevBall.radius + newDY < 0;
    }

    const hitSideWalls = (prevBall, newDX) => {
        return (
            prevBall.position.x + prevBall.radius + prevBall.speed.dx > WORLD_WIDTH ||
            prevBall.position.x - prevBall.radius + newDX < 0
        );
    }

    const handleBallPaddleCollision = (ball, paddle, dx, dy) => {
        const getHitAngel = (padLocalCollisionPoint) => padLocalCollisionPoint * SIXTY_DEGREES_RADIAN;

        const getNewSpeedComponents = (angle) => ({
            newDX: ball.speed.linear * Math.sin(angle),
            newDY: -(ball.speed.linear * Math.cos(angle))
        });

        let collision = null;
        ({ newDX: dx, newDY: dy } = detectPaddleHitBall(paddle, ball, dx, dy));
        collision = detectBallHitPaddle(ball, paddle, dx, dy);
        if(collision !== null) {
            if((collision.wall === "top" && dy > 0) || (collision.wall === "bottom" && dy < 0)) {
                const angle = getHitAngel(collision.x);
                ({ newDX: dx, newDY: dy } = getNewSpeedComponents(angle));
            }
        }

        return { newDX: dx, newDY: dy };
    }

    const handleBlockCollision = (ball, dx, dy, level) => {
        let collisions = [];
        level.forEach( (row, rowIndex) => {
            row.forEach( (column, columnIndex) => {
                var left = columnIndex * 60;
                var top = rowIndex * 30;
                const pRect = {
                    left: left,
                    top: top,
                    right: left + 60,
                    bottom: top + 30
                }
                const p1 = { x: ball.position.x, y: ball.position.y };
                const q1 = { x: p1.x + dx, y: p1.y + dy };

                if(column !== 0) {
                    var collision = checkCollisionBallRect(ball, dx, dy, p1, q1, pRect, ({x: left + 30, y: top + 15}), 30, 15);
                    if(collision != null) { 
                        if(collision.wall === "bottom" || collision.wall === "top") dy = -dy;
                        if(collision.wall === "left" || collision.wall === "right") dx = -dx;
                        collisions.push(({columnIndex, rowIndex}));
                    }
                }
            })
        });

        return { newDX: dx, newDY: dy, collisions };
    }

    const detectBallHitPaddle = (ball, paddle, dx, dy) => {
        const paddleHeightHalf = paddle.size.height / 2;
        const paddleWidthHalf = paddle.size.width / 2;

        const pRect = {
            left: paddle.position.x - paddleWidthHalf,
            right: paddle.position.x + paddleWidthHalf,
            top: paddle.position.y - paddleHeightHalf,
            bottom: paddle.position.y + paddleHeightHalf
        };
    
        const p1 = { x: ball.position.x, y: ball.position.y };
        const q1 = { x: p1.x + dx, y: p1.y + dy };
    
        return checkCollisionBallRect(ball, dx, dy, p1, q1, pRect, paddle.position, paddleWidthHalf, paddleHeightHalf);
    }

    const detectPaddleHitBall = (paddle, ball, dx, dy) => {

        const paddleHeightHalf = paddle.size.height / 2;
        const paddleWidthHalf = paddle.size.width / 2;

        const rect = {
            left: paddle.position.x - paddleWidthHalf,
            right: paddle.position.x + paddleWidthHalf,
            top: paddle.position.y - paddleHeightHalf,
            bottom: paddle.position.y + paddleHeightHalf
        };
        
        let ndx = dx;

        if( 
            ball.position.x + ball.radius > rect.left && ball.position.x - ball.radius < rect.right &&
            ball.position.y + ball.radius > rect.top && ball.position.y - ball.radius < rect.bottom
        ) 
        {
            let balldir = dx < 0 ? -1:1
            let paddledir = paddle.speed < 0 ? -1:1
            let newPositionX = 0;
            let newPositionY = 0;
            ndx = (balldir === paddledir) ? dx:-dx;
            ndx *= 1.2;

            if(ball.position.x < paddle.position.x) { 
                newPositionX = Math.max(rect.left - ball.radius - Math.abs(dx), ball.radius + 1);
                //newPositionY = rect.bottom + ball.radius + 1;
            }
            if(ball.position.x > paddle.position.x) {
                newPositionX = Math.min(rect.right + ball.radius + Math.abs(dx), WORLD_WIDTH - ball.radius - 1);
            }
            setBall(pb => {
                return {
                    ...pb,
                    position: {
                        ...pb.position,
                        x: newPositionX,
                    },
                    speed: {
                        ...pb.speed,
                        dx: ndx,
                    }
                }
            })
        }

        return {newDX: ndx, newDY: dy};
    }

    const checkCollisionBallRect = (circle, dx, dy, p1, q1, pRect, blockPos, blockWidthHalf, blockHeightHalf) => {
        let wall = "";
    
        if(blockRightWallIntersect(p1, q1, pRect, circle)) wall = "right";
        else if(blockLeftWallIntersect(p1, q1, pRect, circle)) wall = "left";
        else if(blockTopWallIntersect(p1, q1, pRect, circle)) wall = "top";
        else if(blockBottomWallIntersect(p1, q1, pRect, circle)) wall = "bottom";
        
        return (wall === "") ? null:{
            x: (q1.x - blockPos.x) / blockWidthHalf,
            y: (q1.y - blockPos.y) / blockHeightHalf,
            dx: dx,
            dy: dy,
            wall: wall
        };
    }
    
    return [step];
}
function paddleNewX(paddle) {
    let pnx = paddle.position.x + paddle.speed;
    if(pnx < paddle.size.width / 2) pnx = paddle.size.width / 2;
    if(pnx > WORLD_WIDTH - (paddle.size.width / 2)) pnx = WORLD_WIDTH - (paddle.size.width / 2);
    return pnx;
}

