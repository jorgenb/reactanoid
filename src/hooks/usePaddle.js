import { useState } from "react"
import { WORLD_HEIGHT, WORLD_WIDTH } from "../worldCfg";

export const usePaddle = () => {

    const MAX_PADDLE_SPEED = 20;

    const [paddle, setPaddle] = useState(() => ({
        size: {
            width: 100,
            height: 15
        },
        position: {
            x: Math.floor(WORLD_WIDTH / 2),
            y: WORLD_HEIGHT - 150
        },
        speed: 0
    }))

    const getNewSpeed = (speed, dir) => {
        let newSpeed = speed;

        if (newSpeed <= 0) newSpeed = 1;
        newSpeed += 1;
        if (newSpeed > MAX_PADDLE_SPEED) newSpeed = MAX_PADDLE_SPEED;

        newSpeed = dir * newSpeed;

        return newSpeed;
    }

    const getNewPosX = (posX, speed, width) => {
        let newPosX = posX + speed;
        const halfsize = width / 2;

        if (newPosX < halfsize) newPosX = halfsize;
        if (newPosX > (WORLD_WIDTH - halfsize)) newPosX = WORLD_WIDTH - halfsize;

        return newPosX;
    }

    const movePaddle = dir => {
        setPaddle(prev => {
            const speed = getNewSpeed(Math.abs(prev.speed), dir);
            const newPosX = getNewPosX(prev.position.x, speed, prev.size.width);

            return {
                ...prev,
                position: {
                    ...prev.position,
                    x: newPosX,
                },
                speed: speed
            }
        });
        return
    }

    const updatePaddleSpeed = dir => {
        setPaddle(prev => {
            const speed = getNewSpeed(Math.abs(prev.speed), dir);
            return {
                ...prev,
                speed: speed
            }
        });
    }

    return [paddle, setPaddle, movePaddle];
}