import { useState } from "react"
import { levels } from "../data/levels";

export const useLevel = () => {
    const [level, setLevel] = useState(() => {
        return levels[0];
    });

    return [level, setLevel];
}