import { useRef, useState } from "react"
import { WORLD_WIDTH } from "../worldCfg";

export const useBall = (_paddle) => {

    const paddleRef = useRef();
    paddleRef.current = _paddle;

    const [ball, setBall] = useState(() => {
        const radius = 11
        return {
            radius: radius,
            position: {
                x: Math.floor(WORLD_WIDTH / 2),
                y: _paddle.position.y - (_paddle.size.height / 2) - radius
            },
            speed: {
                dx: 3,
                dy: -4,
                linear: 5, // linear speed = sqrt(dx² + dy²) : pytagoras
            },
        }
    });

    const resetBall = pos => {
        const radius = 11;
        setBall({
            radius: radius,
            position: {
                x: pos.x + radius,
                y: pos.y - (_paddle.size.height / 2) - radius
            },
            speed: {
                dx: 3,
                dy: -4,
                linear: 5, // linear speed = sqrt(dx² + dy²) : pytagoras
            }
        });
    };

    return [ball, setBall, resetBall];
}

